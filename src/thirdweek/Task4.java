package thirdweek;

import java.util.Scanner;

// Дана строка s.
// Вычислить количество символов в ней,
// не считая пробелов (необходимо использовать цикл).
public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        int symbolCount = 0;
        for (int i = 0; i < input.length(); i++) {
            if (input.charAt(i) != ' ') {
                symbolCount++;
            }
        }
        System.out.println("Результат работы: " + symbolCount);
    }
}
