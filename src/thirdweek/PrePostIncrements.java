package thirdweek;

public class PrePostIncrements {
    public static void main(String[] args) {
        //post increment:
        int i = 1;
        int a = i++;

        int temp = i;
        i = i + 1;
        a = temp;

        System.out.println("i = " + i);
        System.out.println("a = " + a);

        //pre-increment
        int j = 1;
        int b = j++;
        int c = ++j;
        System.out.println("j = " + j);
        System.out.println("b = " + b);
        System.out.println("c = " + c);

    }
}
