package fourthweek;

import java.util.Arrays;
import java.util.Scanner;

/*
   На вход подается число N - длина массива.
   Затем подается массив целых чисел из N элементов.

   Нужно циклически сдвинуть элементы на 1 влево.

   Входные данные:
   5
   1 2 3 4 7
   Выходные данные:
   2 3 4 7 1
   */
public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int[] array = new int[n];
        for (int i = 0; i < n; i++) {
            array[i] = scanner.nextInt();
        }

        //через цикл
        int temp = array[0];
        for (int i = 0; i < n - 1; i++) {
            array[i] = array[i + 1];
        }
        array[array.length - 1] = temp;
        System.out.println(Arrays.toString(array));

        //решение через arraycopy
        int first = array[0];
        System.arraycopy(array, 1, array, 0, array.length - 1);
        array[array.length - 1] = first;
        System.out.println(Arrays.toString(array));
    }
}
