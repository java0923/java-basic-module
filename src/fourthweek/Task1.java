package fourthweek;

import java.util.Scanner;

/*
    На вход подается число N — длина массива.
    Затем передается массив целых чисел длины N.
    Вывести все четные элементы массива.
    Если таких элементов нет, вывести -1.

5
1 2 3 4 5
->
2 4
 */
public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] arr = new int[n];
        boolean flag = false;
        //Заполняем массив пользовательскими данными
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
        }

        for (int i = 0; i < n; i++) {
            if (arr[i] % 2 == 0) {
                System.out.println("Элемент массива четный: " + arr[i]);
                flag = true;
            }
        }
        if (!flag) {
            System.out.println("Четных элементов нет: " + -1);
        }
    }
}
