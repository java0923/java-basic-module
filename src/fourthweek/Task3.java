package fourthweek;

import java.util.Scanner;

/*
На вход подается число N — длина массива.
Затем передается массив целых чисел длины N.
Проверить, является ли он отсортированным массивом строго по убыванию.
Если да, вывести true, иначе вывести false.

5
5 4 3 2 1
->
true

2
43 46
->
false

3
5 5 5
->
false
 */
public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
//        boolean result = false;
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
        }
//        System.out.println(SortArrayChecker.checkIfArraySortedDesc(arr));
        System.out.println(checkIfArraySortedDesc(arr));

//        for (int i = 0; i < n - 1; i++) {
//            if (arr[i] <= arr[i + 1]) {
//                result = false;
//                break;
//            }
//        }

//        boolean result = checkIfArraySortedDesc(arr);
//        System.out.println(result);
    }

    static boolean checkIfArraySortedDesc(int[] inputArray, int n) {
        for (int i = 0; i < n - 1; i++) {
            if (inputArray[i] <= inputArray[i + 1]) {
                return false;
            }
        }
        return true;
    }

    static boolean checkIfArraySortedDesc(int[] inputArray) {
        return checkIfArraySortedDesc(inputArray, inputArray.length);
    }
}
