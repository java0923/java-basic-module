package fifthweek;

import java.util.Scanner;

/*
Развернуть строку рекурсивно.

abcde -> edcba
 */
public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();

        System.out.println(reverseString(s));
    }

    public static String reverseString(String input) {
        if (input.isEmpty()) {
            return input;
        }
        else {
            System.out.println("input.charAt(0): " + input.charAt(0));
            System.out.println("input.substring(1)): " + input.substring(1));
            System.out.println("input.substring(1)) + input.charAt(0): " + input.substring(1) + input.charAt(0));
            return reverseString(input.substring(1)) + input.charAt(0);
        }
    }
}
