package fifthweek;

import java.util.Scanner;

/*
На вход подается число N — ширина и высота матрицы.
Необходимо заполнить матрицу 1 и 0 в виде шахматной доски.
Нулевой элемент должен быть 0.

Входные данные
3
Выходные данные
0 1 0
1 0 1
0 1 0

Входные данные
4
Выходные данные
0 1 0 1
1 0 1 0
0 1 0 1
1 0 1 0
*/
public class Task6 {
    public static void main(String[] args) {
        int n = new Scanner(System.in).nextInt();

        int[][] array = new int[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
//                if ((i + j) % 2 == 0) {
//                    array[i][j] = 0;
//                } else {
//                    array[i][j] = 1;
//                }
                if ((i + j) % 2 != 0) {
                    array[i][j] = 1;
                }
            }
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
    }
}
