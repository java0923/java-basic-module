package fifthweek;

import java.util.Scanner;

/*
Транспонировать матрицу.
Транспонирование — в линейной алгебре это операция над матрицами в результате которой
матрица поворачивается относительно своей главной диагонали.
При этом столбцы исходной матрицы становятся строками результирующей.

Входные данные
4 2 - размерность массива
Сам массив:
1 2
3 4
5 6
7 8
Выходные данные
1 3 5 7
2 4 6 8

Входные данные
2 2 - размерность массива
Сам массив:
2 3
4 5
Выходные данные
2 4
3 5
     */
public class Task9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = scanner.nextInt();

        int[][] array = new int[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                array[i][j] = scanner.nextInt();
            }
        }

        int result[][] = new int[m][n];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                result[j][i] = array[i][j];
            }
        }

        for (int[] ints : result) {
            for (int anInt : ints) {
                System.out.print(anInt + " ");
            }
            System.out.println();
        }
    }
}
