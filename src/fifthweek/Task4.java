package fifthweek;

import java.util.Scanner;

/*
   Написать функцию через рекурсию для вычисления суммы заданных положительных целых чисел a b
   без прямого использования оператора +.

   a + b - нельзя
    */
public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        System.out.println("Сумма введенных чисел: " + sumRecursively(a, b));
    }

    private static int sumRecursively(int a, int b) {
        if (b == 0) {
            return a;
        }
       return sumRecursively(a + 1, b - 1);
    }
}
