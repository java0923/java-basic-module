package fifthweek;

import java.util.Scanner;

/*
На вход подается натуральное число N.
Необходимо проверить, является ли оно степенью двойки (решить через рекурсию).
Вывести true, если является и false иначе.

4 -> true
5 -> false
6 -> false
7 -> false
8 -> true
 */
public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            int n = scanner.nextInt();
            System.out.println(checkPowerOfTwoRecursive(n));
        }
    }

    private static boolean checkPowerOfTwoRecursive(int n) {
        System.out.println("Наше число N: " + n);
        if (n == 2 || n == 1) {
            return true;
        }
        if (n <= 0 || n % 2 != 0) {
            return false;
        }
        return checkPowerOfTwoRecursive(n / 2);
    }
}
