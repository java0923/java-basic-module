package elevenweek.nio;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/*
Создать папку и подпаку в src/
 */
public class Task2 {
    public static void main(String[] args) throws IOException {
        Path path1 = Paths.get("src/elevenweek/nio/newFolder");
        Path path2 = Paths.get("src/elevenweek/nio/newFolder/newSubFolder");
//        Files.createDirectory(path1);
//        Files.createDirectory(path2);
        Files.createDirectories(path2);
    }
}
