package elevenweek.nio;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

/*
Копировать файл с контентом
 */
public class Task3 {
    public static void main(String[] args) throws IOException {
        Path sourcePath = Paths.get("src/elevenweek/nio/test.txt");
        Path targetPath = Paths.get("src/elevenweek/nio/testTarget.txt");

        Path path = Files.copy(sourcePath, targetPath, StandardCopyOption.REPLACE_EXISTING);
        System.out.println("Target file path: " + path);
        System.out.println("Copied content: \n" + new String(Files.readAllBytes(path)));
    }
}
