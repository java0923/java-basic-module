package elevenweek.functional.task4;

/*
Создать параметризованный функциональный интерфейс (Generics)
На выходе получить:
1) Строку наоборот
2) Факториал числа
 */
public class Main {
    public static void main(String[] args) {
        //reverse
        MySuperInterface<String> reverseString = (str) -> new StringBuilder(str).reverse().toString();
        System.out.println(reverseString.func("LAMBDA"));

        //factorial
        MySuperInterface<Integer> factorial = (Integer n) -> {
            int result = 1;
            for (int i = 1; i <= n; i++) {
                result *= i;
            }
            return result;
        };
        System.out.println(factorial.func(5));
    }
}
