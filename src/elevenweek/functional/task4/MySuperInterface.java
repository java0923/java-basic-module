package elevenweek.functional.task4;

@FunctionalInterface
public interface MySuperInterface<T> {
    T func(T value);
}
