package elevenweek.functional.task3;

/*
реализовать метод, чтобы вывести строку наоборот, используя наш ReverseInterface
 */
public class Main {
    private static final String INPUT_STRING = "LAMBDA";

    public static void main(String[] args) {
        //System.out.println(new StringBuilder(INPUT_STRING).reverse());
        ReverseInterface reverseInterface = (str) -> new StringBuilder(str).reverse().toString();
        ReverseInterface reverseInterface1 = (String str) -> {
            StringBuilder stringBuilder = new StringBuilder(str);
            return stringBuilder.reverse().toString();
        };

        System.out.println(reverseInterface.getReverseString(INPUT_STRING));
        System.out.println(reverseInterface1.getReverseString(INPUT_STRING));

        ReverseInterface reverseInterface3 = (str) -> {
            String result = "";
            for (int i = str.length() - 1; i >= 0; i--) {
                result += str.charAt(i);
            }
            return result;
        };
        System.out.println(reverseInterface3.getReverseString(INPUT_STRING));
    }
}
