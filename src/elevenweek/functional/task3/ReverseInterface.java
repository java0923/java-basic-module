package elevenweek.functional.task3;

@FunctionalInterface
public interface ReverseInterface {
    String getReverseString(String inputString);
}
