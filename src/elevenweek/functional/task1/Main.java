package elevenweek.functional.task1;

/*
С помощью функционального интерфейса выполнить подсчет квадрата числа
 */
public class Main {
    public static void main(String[] args) {
        Square square = new Square() {
            @Override
            public int calculateSquare(int x) {
                return x * x;
            }
        };
        System.out.println(square.calculateSquare(3));

        Square lambdaSquare = (x) -> x * x;

        //TODO: method reference
        //System.out.println(lambdaSquare::calculateSquare);
        System.out.println(lambdaSquare.calculateSquare(4));
    }
}
