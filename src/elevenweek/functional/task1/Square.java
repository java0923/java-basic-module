package elevenweek.functional.task1;

@FunctionalInterface
public interface Square {
    int calculateSquare(int x);
}
