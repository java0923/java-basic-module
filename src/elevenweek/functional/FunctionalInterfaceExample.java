package elevenweek.functional;

public class FunctionalInterfaceExample {
    //SAM (Single Abstract Methods) before Java 8
    public static void main(String[] args) {
        //анонимный класс
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Я только что реализовал функциональный интерфейс");
            }
        }).start();
        //SAM (Single Abstract Methods) before Java 8 => (<parameter list>) -> lambda body
        new Thread(() -> System.out.println("Я только что реализовал функциональный интерфейс"))
                .start();
    }

}
