package elevenweek.functional.task2;

@FunctionalInterface
public interface PiInterface {
    double getPiValue();
}
