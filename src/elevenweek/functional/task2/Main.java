package elevenweek.functional.task2;

/*
получить значение PI через собственный функциональный интерфейс
 */
public class Main {
    public static void main(String[] args) {
        PiInterface piInterface;
        piInterface = () -> Math.PI;
        //piInterface = () -> 3.14;
        System.out.println(piInterface.getPiValue());

        //method reference
        piInterface = Math::random;
        System.out.println(piInterface.getPiValue());
    }
}
