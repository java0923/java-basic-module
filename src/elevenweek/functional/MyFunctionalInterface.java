package elevenweek.functional;

@FunctionalInterface
public interface MyFunctionalInterface {
    double getValue();
}
