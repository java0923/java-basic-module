package elevenweek.streams;

import java.util.List;

public class StreamExample {
    public static void main(String[] args) {
        //Потребитель (Consumer)
        //Поставщик (Supplier)
        //Предикат (Predicate)
        //Функция (Function)
        List<String> myPlaces = List.of("Nepal, Pokhara", "Nepal, Kathmandu",  "India, Delhi", "USA, New York", "Africa, Nigeria");
        myPlaces.stream()
                .filter((place) -> place.startsWith("Nepal"))
//                .map((place) -> place.toUpperCase())
                .map(String::toUpperCase)
                .sorted()
//                .forEach((place) -> System.out.println(place));
                .forEach(System.out::println);










    }
}
