package elevenweek.streams.task1;

import elevenweek.functional.task1.Square;

import java.util.ArrayList;
import java.util.List;

/*
Использовать реализованный функциональный интерфейс Square на списке чисел, вывести на экран.
 */
public class Main {
    public static void main(String[] args) {
        List<Integer> numbers = new ArrayList<>();
        numbers.add(3);
        numbers.add(10);
        Square square = (x) -> x * x;

        numbers.stream()
                //.map(num -> square.calculateSquare(num))
                .map(square::calculateSquare)
//                .forEach(num -> System.out.println(num * num));
                .forEach(System.out::println);
    }
}
