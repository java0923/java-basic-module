package firstweek;

import java.util.Scanner;

/*
 Дано двузначное число. Вывести сначала левую цифру (единицы), затем правую (десятки)
*/
public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int c = scanner.nextInt();
        System.out.println("Результат: 2 число = " + c % 10 + "; 1 число = " + c / 10);
    }
}
