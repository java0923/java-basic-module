package secondweek;

import java.util.Scanner;

/*
Дано число n. Нужно проверить его четность.
 */
public class Task1 {

    public static void main(String[] args) {

        int sum = 0;
        for (int odd = 1; odd <= 8; odd += 2) {
            System.out.println("odd iter: " + odd);
            sum += odd;
            System.out.println("sum: " + sum);
        }

//        Scanner scanner = new Scanner(System.in);
//        int n = scanner.nextInt();
//        if (n % 2 == 0) {
//            System.out.println("Четное число");
//        } else {
//            System.out.println("Нечетное число");
//        }
//
////        if (n % 2 != 0) {
////            System.out.println("Нечетное число");
////        }
//
//        //тернарный оператор (пример)
//        String str;
//        str = (n % 2 == 0) ? " число четное " : " нечетное число ";
//        System.out.println(str);
//        System.out.println((n % 2 == 0) ? " число четное " : " нечетное число ");
    }
}
