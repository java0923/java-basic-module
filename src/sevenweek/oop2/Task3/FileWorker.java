package sevenweek.oop2.Task3;

import java.io.*;
import java.util.Scanner;

public class FileWorker {
    private static final String FILE_DIRECTORY = "/Users/andreigavrilov/Work/Projects/СберКурсыШкола/Java0910/JavaBasic/src/sevenweek/oop2/Task3/files";
    private static final String OUTPUT_FILE_NAME = "output.txt";

    private FileWorker() {
    }

    public static void readAndWriteDataFromFile(String filePath) throws IOException {
        Scanner scanner = new Scanner(new File(filePath));
        String[] days = new String[10];
        int i = 0;
        while (scanner.hasNextLine()) {
            days[i++] = scanner.nextLine();
        }

        Writer writer = new FileWriter(FILE_DIRECTORY + "/" + OUTPUT_FILE_NAME);
        for (int j = 0; j < i; j++) {
            String result = "Порядковый номер дня недели " + days[j] + " = " +
                    WeekDays.ofName(days[j]).dayNumber + "\n";
            writer.write(result);
        }
        writer.close();
        scanner.close();
    }
}
