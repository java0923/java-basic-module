package sevenweek.oop2.Task1;


import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Objects;

public class VariableLength {

    static int sum(int... numbers) {
        int res = 0;
        for (int i = 0; i < numbers.length; i++) {
            res += numbers[i];
        }
        return res;
    }

    static boolean findChar(Character ch, String... strings) {
        for (int i = 0; i < strings.length; i++) {
            if (strings[i].indexOf(ch) != -1) {
                return true;
            }
        }
        return false;

    }

//    static int sum1(Collection<Integer> input) {
//
//    }

    public static void main(String[] args) {
        int[] n = new int[10];
        System.out.println(sum(n));
        System.out.println(sum(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
        System.out.println(findChar('a', "mother", "python", "father"));
        System.out.println(String.format("This is digit %d. This is String %s", 123, "hi"));
    }
}
