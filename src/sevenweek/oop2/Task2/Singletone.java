package sevenweek.oop2.Task2;

/*
Синглтон - паттерн, дающий гарантию, что:
1) Класс будет создан в единственном экземпляре
2) Предоставляет глобальную точку доступа к экземпляру класса

Самая простая реализация:
+
Простота и прозрачность кода
Высокая производительность в многопоточной среде
Потокобезопасность

-
Не ленивая инициализация
 */

public class Singletone {
    private static final Singletone INSTANCE = new Singletone();

    private Singletone() {

    }

    public static Singletone getInstance() {
        return INSTANCE;
    }

}
