package sevenweek.oop2.Task2;
/*
Ленивая инициализация
+
Простота и прозрачность кода
Ленивая инициализация

-
Не потокобезопасно
 */
public class LazySingletone {

    private static LazySingletone INSTANCE;

    private LazySingletone() {

    }

    public static LazySingletone getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new LazySingletone();
        }
        return INSTANCE;
    }
}
