package sevenweek.oop2.Task2;

/*
Ленивый потокобезопасный синглтон

+
Простота и прозрачность кода
Ленивая инициализация
Потокобезопасность

-
Низкая производительность в многопоточной среде
 */
public class LazySyncSingletone {
    private static LazySyncSingletone INSTANCE;

    private LazySyncSingletone() {

    }

    public static synchronized LazySyncSingletone getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new LazySyncSingletone();
        }
        return INSTANCE;
    }
}
