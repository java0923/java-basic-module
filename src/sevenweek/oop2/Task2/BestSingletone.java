package sevenweek.oop2.Task2;

/*
Double Checked Locking Singletone
 */
public class BestSingletone {
    private static BestSingletone INSTANCE;

    private BestSingletone() {
    }

    public static BestSingletone getInstance() {
        if (INSTANCE == null) {
            synchronized (BestSingletone.class) {
                if (INSTANCE == null) {
                    INSTANCE = new BestSingletone();
                }
            }
        }
        return INSTANCE;
    }
}
