package sevenweek.oop2.Task4;

import java.util.Arrays;

public class SimpleArrayList {
    private static int DEFAULT_CAPACITY = 3;
    private int[] arr;
    private int size;
    private int capacity;

    public SimpleArrayList() {
        arr = new int[DEFAULT_CAPACITY];
        capacity = DEFAULT_CAPACITY;
        size = 0;
    }

    public SimpleArrayList(int capacity) {
        arr = new int[capacity];
        this.capacity = capacity;
        size = 0;
    }

    public void add(int elem) {
        //1,2 -> capacity = 2; size = 2;
        if (size >= capacity) {
            capacity = 2 * capacity;
            arr = Arrays.copyOf(arr, capacity);
        }
        arr[size] = elem;
        size++;
    }

    public int get(int index) {
        if (index < 0 || index > size) {
            System.out.println("Impossible action");
            return -1;
        } else {
            return arr[index];
        }
    }

    public int size() {
        return size;
    }

}
