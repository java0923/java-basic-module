package tenweek.annotations.task;

import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/*
Написать аннотацию,
содержащую некоторую информацию о классе:
автор, дата создания класса, номер текущей версии, список ревьюеров.
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
public @interface ClassDescription {
    String author();

    String date();

    int currentRevision() default 1;

    String[] reviewers();
}
