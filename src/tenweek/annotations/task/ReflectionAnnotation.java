package tenweek.annotations.task;

import java.util.Arrays;

/*
Обращение к аннотации с помощью Reflection
Для проверки наличия аннотации у класса, существуют методы класса Class:
isAnnotationPresent()
Проверяет, есть ли у класса данная аннотация
getAnnotation()
Возвращает аннотацию, если она есть у класса, иначе null
getAnnotations()
Возвращает все аннотации, примененные к классу
getDeclaredAnnotations()
Как предыдущий, но не учитывает аннотации, унаследованные с помощью @Inherited
 */
public class ReflectionAnnotation {
    public static void main(String[] args) {
        printClassAnnotation(PerfectClass.class);
    }

    public static void printClassAnnotation(Class<?> cls) {
        if (!cls.isAnnotationPresent(ClassDescription.class)) {
            return;
        }
        ClassDescription classDescription = cls.getAnnotation(ClassDescription.class);
        System.out.println("Автор: " + classDescription.author());
        System.out.println("Дата: " + classDescription.date());
        System.out.println("Версия: " + classDescription.currentRevision());
        System.out.println("Ревьюеры: " + Arrays.toString(classDescription.reviewers()));
    }
}
