package tenweek.reflection.classname;

import java.util.AbstractMap;
import java.util.HashMap;

/*
Получение имени класса их Class
Имеется несколько методов для получения различных вариантов имени класса:
 - getName()
Полное имя класса (java.lang.Integer)
 - getSimpleName()
Короткое имя без пакета (Integer)
 - getPackage().getName()
Получить только имя пакета (java.lang)
 */
public class ReflectionClassName {
    public static void main(String[] args) {
        //printNameForClass(int.class, "int.class (primitives)");
        printNameForClass(String.class, "String.class");
        printNameForClass(java.util.HashMap.SimpleEntry.class, "HashMap.class (nested class)");
        printNameForClass(new java.io.Serializable() {}.getClass(),
                "new java.io.Serializable() {}.getClass() (anonymous inner class");
    }

    public static void printNameForClass(Class<?> clazz,
                                         String label) {
        System.out.println(label + ": ");
        System.out.println("getName(): " + clazz.getName());
        System.out.println("getSimpleName(): " + clazz.getSimpleName());
        System.out.println("getPackage().getName(): " + clazz.getPackage().getName());
        System.out.println("getType(): " + clazz.getTypeName());
        System.out.println();
    }
}
