package tenweek.reflection.methods;

import tenweek.reflection.modifiers.Task4;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/*
Работа с методами
Методы для получения методов похожи на те, что были для полей:
getMethods(),
getDeclaredMethods(),
getMethod(),
getDeclaredMethod()
Смысл абсолютно тот же, только возвращаются методы

Основные методы класса Method:
getModifiers()
getName()
newInstance() - Создаёт новый инстанс класса, вызвав конструктор

Работа с конструкторами
Аналогично:
getConstructors
getDeclaredConstructors
getConstructor
getDeclaredConstructor
 */
public class ReflectionMethods {
    public static void main(String[] args) {
        //сконструировать класс (то есть, вызвать конструктор класса, и обработать все возможные исключения)
        Class<Task5> cls = Task5.class;
        Task5 task5 = new Task5(1, "test initial");
        System.out.println(task5);
        try {
            Constructor<Task5> constructor = cls.getDeclaredConstructor(int.class, String.class);
            Task5 result = constructor.newInstance(143, "Test Constructor Reflection");
            System.out.println(result);
            System.out.println(result.a);
            System.out.println(result.b);
        } catch (NoSuchMethodException | InvocationTargetException | InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
}
