package tenweek.reflection.methods;

public class Task5 {
    int a;
    String b;

    public Task5(int a, String b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public String toString() {
        return "Task5{" +
                "a=" + a +
                ", b='" + b + '\'' +
                '}';
    }
}
