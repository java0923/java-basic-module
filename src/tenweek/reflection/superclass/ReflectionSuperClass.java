package tenweek.reflection.superclass;
/*
Получение родителей
 - метод getSuperClass() - возвращает Class родителя текущего класса
 - метод getInterfaces() - возвращает список Class'ов интерфейсов, реализуемых текущим классом
 */

/*
interface A extends U {}
interface B {}
interface U {}
class C implements A {}
class D extends C implements B {}
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ReflectionSuperClass {
    public static void main(String[] args) {
        //вывести для класса D все интерфейсы
//        for (Class<?> cls : D.class.getInterfaces()) {
//            System.out.println(cls.getName());
//        }
        /*
        Задача 1
        Получить все интерфейсы класса, включая интерфейсы от классов-родителей.
        Не включать интерфейсы родительских интерфейсов.
         */
        List<Class<?>> res = getAllInterfaces(D.class);
        for (Class<?> cls : res) {
            System.out.println(cls.getName());
        }

    }

    public static List<Class<?>> getAllInterfaces(Class<?> cls) {
        List<Class<?>> interfaces = new ArrayList<>();
        while (cls != Object.class) {
            interfaces.addAll(Arrays.asList(cls.getInterfaces()));
            cls = cls.getSuperclass();
        }
        return interfaces;
    }
}
