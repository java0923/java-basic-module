package tenweek.reflection.modifiers;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

/*
Получение модификаторов класса
 - getModifiers() - все модификаторы, с которыми был объявлен класс
 public native int getModifiers();

Модификаторы пакуются внутри битов инта, распаковать информацию можно методами класса Modifier:
public static boolean isPublic(int mod) { }
public static boolean isPrivate(int mod) { }
public static boolean isProtected(int mod) { }
public static boolean isStatic(int mod) { }
public static boolean isFinal(int mod) { }
public static boolean isInterface(int mod) { }
public static boolean isAbstract(int mod) { }

Работа с полями:
Метод getFields() возвращает все публичные поля класса или интерфейса, включая унаследованные:
public Field[] getFields() throws SecurityException

Метод getDeclaredFields() возвращает вообще все поля класса или интерфейса, но исключая унаследованные:
public Field[] getDeclaredFields() throws SecurityException

*SecurityException бросается в случае запрета доступа к пакету, в котором лежит класс

Есть еще методы getField() и getDeclaredField() позволяют найти поле по его строчному имени.

Основные методы класса Field:
get() и set() позволяют прочитать и записать значение поля:
getName() возвращает имя поля
getType() возвращает объект Class для его типа
getModifiers() тоже есть и работает так же, как для Class
 */
public class ReflectionModifiers {
    public static void main(String[] args) throws IllegalAccessException {
        // Вывести все поля класса, их модификаторы и типы.
        Task4 task4 = new Task4();
        task4.i = 987;
        printAllFields(Task4.class, task4);
    }

    public static void printAllFields(Class<?> clazz, Task4 task4) throws IllegalAccessException {
        for (Field field : clazz.getDeclaredFields()) {
            int mods = field.getModifiers();
            if (Modifier.isPublic(mods)) {
                System.out.print(" public ");
            }
            if (Modifier.isProtected(mods)) {
                System.out.print(" protected ");
            }
            if (Modifier.isPrivate(mods)) {
                System.out.print(" private ");
                field.setAccessible(true);
            }
            if (Modifier.isStatic(mods)) {
                System.out.print(" static ");
            }
            if (Modifier.isFinal(mods)) {
                System.out.print(" final ");
            }
            System.out.print(" " + field.getType().getName() + " " +
                    field.getName() + ": " + field.get(task4));
            System.out.println();
//            field.setAccessible(false);
        }
    }
}
