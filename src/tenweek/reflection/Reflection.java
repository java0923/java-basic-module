package tenweek.reflection;
/*
Определение
Рефлексия (англ. reflection) – знание кода о самом себе
К рефлексии можно отнести возможность проитерироваться по всем полям класса
или найти и создать объект класса, по имени, заданному через текстовую строку

Class – основной класс для рефлексии в Java
 */
public class Reflection {
    public static void main(String[] args) {
        //три способа получить Class
        //1 - псевдо-поле .class
        Class<String> str1 = String.class;

        //2 способ - метод getClass(). Он у класса Object
        CharSequence sequence = "My String";
        Class<? extends CharSequence> seqInfo = sequence.getClass(); //class java.lang.String
        System.out.println(seqInfo);

        //3 способ - поиск класса по строковому литералу
        try {
            Class<?> integerClass = Class.forName("java.lang.Integer");
            System.out.println(integerClass);
        } catch (ClassNotFoundException e) {
            System.out.println("Такого класса нету");
        }
    }
}
