--конкатенация строк
select 'Текущее время: ' || now() || ' сейчас';

--основные математические функции и операции
select 2 + 3 plus,
       2 - 3 minus,
       2 * 3 multiply,
       2 / 3 division,
       5 % 3 remain_division,
       2 ^ 3 degree,
       abs(-2),
       sqrt(9);

select *
from (select book_id, count(*) cnt
      from reviews
      group by book_id
      order by 1) res
where cnt > 1;

select book_id, count(*) cnt
from reviews
group by book_id
having count(*) > 1 --(select ...)
order by 1;

--departments(id, name)
--employees(id, last_name, first_name, department_id, salary)
select id, last_name, department_id, sum(salary)
from employees e
group by id, last_name, department_id
having sum(salary) > (select max(salary) from employees group by department_id);
--where salary < (select min(salary) from employees group by department_id)

-- подзапрос с соединением с другой таблицей
select *
from (select *
      from books b,
           reviews r
      where b.id = r.book_id) pre_res
         right join books b2
                    on pre_res.book_id = b2.id;

-- конструкция with для виртуальноый таблицы
with test as (select *
              from books b,
                   reviews r
              where b.id = r.book_id),
     test1 as (select now())
--test2 as (select * from test)
select *
from test
         right join books b2
                    on test.book_id = b2.id
         cross join test1
;
-- NL - Nested Loops
-- Hash - Hash Join

-- иерархическая структура таблицы и запросы
create table departments
(
    id_department integer primary key,
    v_name        text,
    id_parent     integer references departments (id_department)
);

insert into departments (id_department, v_name, id_parent)
values (1, 'Главный департамент', null),
       (2, 'Департамент 1', 1),
       (3, 'Департамент 2', 1),
       (4, 'Департамент 1-1', 2),
       (5, 'Департамент 2-1', 3),
       (6, 'Департамент 1-2', 2),
       (7, 'Департамент 2-2', 3);

select *
from departments;

select d2.*
from departments d1,
     departments d2
where d1.id_department = d2.id_parent
  and d1.id_parent = 1;

-- ORACLE only
-- select v_name, level, sys_connect_by_path(v_name, '->')
-- from departments
-- --start with id_parent is null
-- connect by prior id_department = id_parent;

with recursive dep as
                   (select id_department, v_name, id_parent
                    from departments
                    where id_parent is null
                    union all
                    select c.id_department, c.v_name, c.id_parent
                    from dep p,
                         departments c
                    where c.id_parent = p.id_department)
select *
from dep;

-- добавим вложенность и текст зависимости
-- explain (analyse)
with recursive dep as
                   (select id_department, v_name, id_parent, 1 as level, v_name as path
                    from departments
                    where id_parent is null
                    union all
                    select c.id_department, c.v_name, c.id_parent, p.level + 1, p.path || ' -> ' || c.v_name
                    from dep p,
                         departments c
                    where c.id_parent = p.id_department)
select *
from dep;


--PL/SQL
/*
CREATE [or REPLACE] FUNCTION/PROCEDURE function_name(param_list)
   RETURNS return_type
   LANGUAGE plpgsql
  as
$$
DECLARE
-- variable declaration
BEGIN
 -- logic
END;
$$
 */

CREATE table users
(
    id         serial primary key,
    name       varchar(255),
    profession varchar(255)
);

insert into users(name, profession)
values ('Bob', 'QA'),
       ('Camilo', 'Front End developer'),
       ('Billy', 'Backend Developer'),
       ('Alice', 'Mobile Developer'),
       ('Kate', 'QA'),
       ('Wayne', 'DevOps'),
       ('Tim', 'Mobile Developer'),
       ('Amigos', 'QA');

CREATE TABLE purchases
(
    id      serial primary key,
    name    varchar(255),
    cost    numeric(10, 2),
    user_id int,
    foreign key (user_id)
        references users (id)
);

insert into purchases(name, cost, user_id)
values ('M1 MacBook Air', 1300.99, 1),
       ('Iphone 14', 1200.00, 2),
       ('Iphon 10', 700.00, 3),
       ('Iphone 13', 800.00, 1),
       ('Intel Core i5', 500.00, 4),
       ('M1 MacBook Pro', 1500, 5),
       ('IMAC', 2500, 7),
       ('ASUS VIVOBOOK', 899.99, 6),
       ('Lenovo', 1232.99, 1),
       ('Galaxy S21', 999.99, 2),
       ('XIAMI REDMIBOOK 14', 742.99, 4),
       ('M1 MacBook Air', 1299.99, 8),
       ('ACER', 799.99, 7);

select *
from purchases;

-- функция, которая покажет стоимость самой дорогой покупки определенного пользователя
create or replace function findMostExpensivePurchase(p_user_id int)
    returns numeric(10, 2)
    language plpgsql
as
$$
declare
    v_itemCost numeric(10, 2);
begin
    select max(cost)
    into v_itemCost
    from purchases
    where user_id = p_user_id;
    --dbms_output.put_line();
    RAISE NOTICE 'My value: %', v_itemCost;
    return v_itemCost;
end;
$$;

select findMostExpensivePurchase(1);

create table accounts
(
    id      SERIAL primary key,
    balance BIGINT,
    user_id INT unique,
    FOREIGN KEY (user_id)
        references users (id)
);

INSERT INTO accounts(balance, user_id)
values (1500, 1),
       (1100, 2),
       (2300, 3),
       (7500, 5),
       (6500, 4);

-- процедура, которая принимает три параметра, и вычитает сумму из одного аккаунта, и добавляет ее к другому
create or replace procedure transfer(p_source_account_id int, p_dest_account_id int, p_amount int)
    language plpgsql
as
$$
begin
    RAISE NOTICE 'Procedure started';
    update accounts
    set balance = balance - p_amount
    where id = p_source_account_id;

    update accounts
    set balance = balance + p_amount
    where id = p_dest_account_id;
    commit;
    RAISE NOTICE 'Procedure finished';
end;
$$;

select *
from accounts;
call transfer(3, 4, 500);

-- тестирование циклов
CREATE or REPLACE PROCEDURE test_loops()
    LANGUAGE plpgsql
as
$$
BEGIN
    for i in 1..10
        loop
            raise notice 'Number in a loop: %', i;
        end loop;
END;
$$;

call test_loops();

-- тестирование циклов
CREATE or REPLACE PROCEDURE test_loops_query()
    LANGUAGE plpgsql
as
$$
declare
    iter record;
BEGIN
    for iter in select * from books
        loop
            raise notice 'Value in a loop: %', iter.title;
        end loop;
END;
$$;
call test_loops_query();

---------------------------------SQL PLANS----------------------------
-- создадим таблицу на миллион значений, сгенерированных
CREATE TABLE foo
(
    c1 integer,
    c2 text
);

INSERT INTO foo
SELECT i, md5(random()::text)
FROM generate_series(1, 1000000) AS i;

select count(*)
from foo;

explain select * from foo;

INSERT INTO foo
SELECT i, md5(random()::text)
FROM generate_series(1, 10) AS i;

explain select * from foo;

analyse foo;

explain select * from foo;
--default_statistics_target

explain (analyse) select * from foo;


explain (analyse)  select *
from books b,
     reviews r
where b.id = r.id;

--TODO: материалы для изучения
/*
 1) https://postgrespro.ru/docs/postgrespro/10/plpgsql-control-structures - процедуры, функции/ циклы
 2) https://postgrespro.ru/docs/postgresql/9.6/plpgsql-trigger - триггеры
 3) https://learndb.ru/articles/article/97 - про PostgreSQL в целом, там и иерархия тоже
 4) https://habr.com/en/articles/169751/ - хинты планировщика запросов
 5) https://habr.com/en/companies/slurm/articles/684826/ - Как ускорить работу PostgreSQL с помощью конфигурации базы и оптимизации запросов
 6) https://postgrespro.ru/docs/postgrespro/10/performance-tips - про оптимизацию запросов
 7) https://habr.com/en/companies/oleg-bunin/articles/319018/ - Производительность запросов в PostgreSQL – шаг за шагом
 8) https://habr.com/en/articles/203320/ - Оптимизация запросов. Основы EXPLAIN в PostgreSQL
 */




















