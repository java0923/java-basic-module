--sql (Structured Query Language), select .....
--dml (Data Manipulation Language), update/insert/merge/delete. COMMIT!
--ddl (Data Definition Language), create... /drop

/*
 Создать таблицу своими руками
 Создать таблицу книг со следующими полями
 - id ключ
 - title- наименование книги
 - author - автор книги
 - date_added - дата добавления книги
 */
create table books
(
    id         serial primary key,
    title      varchar(30) not null,
    author     varchar(30) not null,
    date_added timestamp   not null
);
--create sequence book_seq;
--select nextval('book_seq');
--drop sequence book_seq;
select now(), current_timestamp, current_date, current_time;

select *
from books;

--добавляем данные в таблицу
insert into books(id, title, author, date_added)
values (nextval('books_id_seq'), 'Недоросль', 'Фонвизин', now());

insert into books(title, author, date_added)
values ('Недоросль', 'Фонвизин', now());

--Если не указываем поля в insert, нужно вставлять все данные
insert into books
values (nextval('books_id_seq'), 'Недоросль', 'Фонвизин', now());

/*
Немного про транзакции
ACID свойства транзакции
A - Атомарность
C - Консистентность
I - Изоляция
D - Долговечность
 */

insert into books
values (nextval('books_id_seq'), 'Доктор Живаго', 'Пастернак', now());
-- подтверждение изменений
commit;
-- откат транзакции/изменений
rollback;

--Изменение данных столбца
insert into books(title, author, date_added)
values ('Доктор Живаго', 'Б. Л. Пастернак', now() - interval '24h');
insert into books(title, author, date_added)
values ('Сестра моя - жизнь', 'Б. Л. Пастернак', now() - interval '24h');
--ошибка, не хватает места под название книги
insert into books(title, author, date_added)
values ('Путешествие из Петербурга в Москву', 'А. Н. Радищев', now() - interval '24h');

alter table books
    alter column title type varchar(100);
alter table books
    alter column author type varchar(100);

alter table books
    alter column author type varchar(100) using author::varchar(100);

alter table books
    add column genre varchar;

select current_timestamp, current_timestamp::date, cast(current_timestamp as date);

select *
from books;

--Удаление данных из таблицы
--delete - удаление записей из таблицы с возможность фильтрации (условию)
--drop - полностью удаление всей таблицы и ее данных
--truncate - очистка данных таблицы без условия

delete
from books
where title = 'Доктор Живаго'
  and date_added > now();

truncate table books;

drop table books;

--вернем все, как было
create table books
(
    id         serial primary key,
    title      varchar(100) NOT NULL,
    author     varchar(100) NOT NULL,
    date_added timestamp    NOT NULL
);
INSERT INTO books(title, author, date_added)
VALUES ('Недоросль', 'Д. И. Фонвизин', now());
INSERT INTO books(title, author, date_added)
VALUES ('Путешествие из Петербурга в Москву', 'А. Н. Радищев', now() - interval '24h');
INSERT INTO books(title, author, date_added)
VALUES ('Доктор Живаго', 'Б. Л. Пастернак', now() - interval '24h');
INSERT INTO books(title, author, date_added)
VALUES ('Сестра моя - жизнь', 'Б. Л. Пастернак', now());


update books
set title = 'Путешествие из Петербурга в Москву'
where id = 2;

