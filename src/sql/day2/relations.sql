/*
виды связей:
 1 - 1,
 1 - M, M - 1
 M - M - делится на две M:1 (1:M) - то есть, добавляется третья таблица
книги           авторы          авторы_книг
id              id              id  id_книги  id_автора
1               4               1   1         4
2               5               2   1         6
3               6               3   2         5
 */

create table if not exists reviews
(
    id       serial primary key,
    book_id  integer references books (id),
    reviewer varchar(100) not null,
    rating   integer      not null,
    comment  text         null
);
--получим ошибку, так как нет книги с таким ID
insert into reviews(book_id, reviewer, rating, comment)
values (777, 'Петя', 9, 'отличная книга');

insert into reviews(book_id, reviewer, rating, comment)
values (1, 'Петя', 9, 'отличная книга'),
       (1, 'Кирилл', 10, null),
       (3, 'Петя', 7, 'ok'),
       (4, 'Иннокентий', 2, 'не понравилась');

select *
from reviews;

--Достать только те записи reviews, у которых comment != null
select *
from reviews
where comment is not null;

--Посчитать сколько всего записей в reviews. Назвать столбец Количество отзывов (Элиас)
select count(*) as "Количество отзывов"
from reviews;

--Узнать количество уникальных id книг, по которым были оставлены отзывы:
select count(distinct book_id)
from reviews;

--Вывести сколько review по каждой id книги
select book_id, count(*)
from reviews
group by book_id
order by 1;

--Вывести все значения по books и по reviews (объединение столбцов результатов)
select *--b.title, r.comment, r.reviewer
from books b,
     reviews r
where b.id = r.book_id;

select *
from books b
         inner join reviews r on b.id = r.book_id;

--left/right joins
select *
from books b
         left join reviews r on b.id = r.book_id;
--декартово произведение
select *
from books
         cross join reviews r;

--full join
select *
from books b
         full join reviews r on b.id = r.book_id;

--natural join
select *
from books b
         natural join reviews r;

--Вычислить среднюю оценку по каждой книге. Вывести столбцы Оценка, Название книги.
select b.title "Название книги", avg(r.rating) "Средняя оценка"
from books as b
         join reviews as r on b.id = r.book_id
group by b.title;

--cascade
delete from books
where id = 1;

--1) убрать данные из review
--2) убрать данные из books
-- каскадное удаление/изменение
alter table reviews
    drop constraint reviews_book_id_fkey;

alter table reviews
    add foreign key (book_id) references books
        on delete cascade;

delete from books
where id = 1;