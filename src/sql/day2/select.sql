/*
 1. Достать запись по ее ID (книга)
 */
select id, title--, row_number() over (partition by title order by id) rn
from books;
--order by id desc
limit 3;
-- where id = 2;
--order by - сортировка по-умолчнию asc - по возрастанию, можно по убыванию - desc

 /*
2. Найти автора по книге (по названию)
  */
select *
from books
where title = 'Недоросль';

/*
3. Найти все книги Пастернака
 */
select *
from books
where trim(author) like '%Пас_ернак';

/*
4. Вывести максимальное значение id в таблице
 */
select title, author, count(id) cnt --max(id)
from books
group by title, author
order by title, author;

select --count(*), count(distinct title), count(distinct author),
       id,
       author,
       count(title) over (partition by author order by id)
from books
order by 2;

-- select distinct author
-- from books
-- where author not like '_. _. %';
-- where length(author) <= 5;
--встроенные функции:
--sum, count, avg, min, max, mod, power, lag, lead...

/*
5. Найти все книги Радищева или Пастернака отсортированные по дате в обратном порядке
 */
select *
from books
-- where trim(lower(author)) like '%пастернак'
where trim(author) ilike '%Пастернак'
   or trim(author) ilike '%Радищев'
order by date_added desc;

select *
from (select *
      from books
      where trim(author) ilike '%Пастернак'
--union - сортировка и уникальный набор данных
      union all
      -- без сортировки и "как есть" данные склеивает
      select *
      from books
      where trim(author) ilike '%Радищев') t
order by date_added desc;
--1 2 3 4 5
--2 3 4 6 7
--union = 1 2 3 4 5 6 7
--union all = 1 2 2 3 3 4 4 5 6 7
/*
 EAV (Entity Attribute Value)
 objects(object_id, name, object_type_id)

 object_types(object_type_id, type_name)

 attributes(attr_id, object_id, value)

 attrs(attr_id, name, type)

 attr_object_types(attr_id, object_type_id)
 */
--regexp_like(author, '%Пастернак', i)

/*
6. Найти все книги Пастернака добавленные вчера
 */
select *
from books
where trim(author) ilike '%Пастернак'
and date_added <= now() - interval '24h'

