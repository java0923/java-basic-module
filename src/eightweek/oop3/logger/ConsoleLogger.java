package eightweek.oop3.logger;

import eightweek.oop3.logger.Logger;

public class ConsoleLogger implements Logger {
    @Override
    public void log(String message) {
        System.out.println(message);
    }
}
