package eightweek.oop3.logger.filelog;

import eightweek.oop3.logger.FileOutput;
import eightweek.oop3.logger.Logger;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class FileLogger extends FileOutput implements Logger {
    private String fileName;

    public FileLogger() {
        this.fileName = getFileName() + getFileExtention();
        //default_file_name.txt
    }

    public FileLogger(String fileName) {
        this.fileName = fileName + getFileExtention();
    }

    @Override
    public String getFileExtention() {
        return ".txt";
    }

    @Override
    public void log(String message) {
        try (Writer writer = new FileWriter(fileName, true)) {
            writer.write(message + "\n");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
