package eightweek.oop3.logger;

public abstract class FileOutput {
    private final String fileName = "default_file_name";

    protected String getFileName() {
        return fileName;
    }

    public abstract String getFileExtention();
}
