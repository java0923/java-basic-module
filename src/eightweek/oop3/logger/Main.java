package eightweek.oop3.logger;

import eightweek.oop3.logger.filelog.CsvFileLogger;
import eightweek.oop3.logger.filelog.FileLogger;

public class Main {
    public static void main(String[] args) {
        Logger fileLogger = new FileLogger();
        //fileName = default_file_name.txt
        fileLogger.log("Hello World!");

        Logger consoleLogger = new ConsoleLogger();
        consoleLogger.log("Console logger works! ");

        Logger csvLogger = new CsvFileLogger();
        //fileName = default_file_name.txt
        csvLogger.log("Hello World From CSV!");
    }
}
