package eightweek.oop3.logger;

public interface Logger {
    void log(final String message);
}
