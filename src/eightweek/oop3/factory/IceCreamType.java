package eightweek.oop3.factory;

public enum IceCreamType {
    CHERRY,
    CHOCOLATE,
    VANILLA;
}
