package eightweek.oop3.factory;

public interface IceCream {
    void printIngredients();
}
