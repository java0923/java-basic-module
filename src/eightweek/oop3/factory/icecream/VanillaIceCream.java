package eightweek.oop3.factory.icecream;

import eightweek.oop3.factory.IceCream;

public class VanillaIceCream implements IceCream {
    @Override
    public void printIngredients() {
        System.out.println("Ванилин, молоко, лед");
    }
}
