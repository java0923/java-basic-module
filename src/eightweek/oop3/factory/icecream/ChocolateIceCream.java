package eightweek.oop3.factory.icecream;

import eightweek.oop3.factory.IceCream;

public class ChocolateIceCream implements IceCream {
    @Override
    public void printIngredients() {
        System.out.println("Шоколад, крем, любовь");
    }
}
