package eightweek.oop3.factory.icecream;

import eightweek.oop3.factory.IceCream;

public class CherryIceCream implements IceCream {

    @Override
    public void printIngredients() {
        System.out.println("Крем, лед, вишня, молоко");
    }
}
