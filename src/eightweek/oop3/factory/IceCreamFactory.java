package eightweek.oop3.factory;

import eightweek.oop3.factory.icecream.CherryIceCream;
import eightweek.oop3.factory.icecream.ChocolateIceCream;
import eightweek.oop3.factory.icecream.VanillaIceCream;

/**
 * Реализация паттерна "Фабрика" (Factory)
 */
public class IceCreamFactory {

    public IceCream getIceCream(IceCreamType iceCreamType) {
        IceCream iceCream = null;
        switch (iceCreamType) {
            case CHERRY -> {
                return new CherryIceCream();
            }
            case VANILLA -> {
                return new VanillaIceCream();
            }
            case CHOCOLATE -> {
                return new ChocolateIceCream();
            }
        }
        return iceCream;
    }

    public IceCream getIceCream(Class object) {
        IceCream iceCream = null;
        if (object.equals(ChocolateIceCream.class)) {
            iceCream = new ChocolateIceCream();
        }
        if (object.equals(VanillaIceCream.class)) {
            iceCream = new VanillaIceCream();
        }
        if (object.equals(CherryIceCream.class)) {
            iceCream = new CherryIceCream();
        }
        return iceCream;
    }

    //в данном примере не очень хорошая идея так делать, так как масло масленное
    public IceCream getIceCream(Object object) {
        IceCream iceCream = null;
        if (object instanceof ChocolateIceCream) {
            iceCream = new ChocolateIceCream();
        }
        if (object instanceof VanillaIceCream) {
            iceCream = new VanillaIceCream();
        }
        if (object instanceof CherryIceCream) {
            iceCream = new CherryIceCream();
        }
        return iceCream;
    }
}
