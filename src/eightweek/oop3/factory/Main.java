package eightweek.oop3.factory;

import eightweek.oop3.factory.icecream.CherryIceCream;
import eightweek.oop3.factory.icecream.ChocolateIceCream;

public class Main {
    public static void main(String[] args) {
//        CherryIceCream cherryIceCream = new CherryIceCream();
//        cherryIceCream.printIngredients();

//        IceCream chocolateIceCream = new ChocolateIceCream();
//        chocolateIceCream.printIngredients();

        IceCreamFactory iceCreamFactory = new IceCreamFactory();
        //Хорошая реализация
        IceCream myCherryIceCream = iceCreamFactory.getIceCream(IceCreamType.CHERRY);
        IceCream myVailaIceCream = iceCreamFactory.getIceCream(IceCreamType.VANILLA);
        IceCream myChololateIceCream = iceCreamFactory.getIceCream(IceCreamType.CHOCOLATE);

        myChololateIceCream.printIngredients();
        myCherryIceCream.printIngredients();
        myVailaIceCream.printIngredients();

        //так не делаем! Плохой пример!
        IceCream myIceCream = iceCreamFactory.getIceCream(new CherryIceCream());
        //IceCream myIceCream1 = new CherryIceCream();

        IceCream iceCream = iceCreamFactory.getIceCream(CherryIceCream.class);
    }
}
