package eightweek.oop3.arraylist;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        //Car volvo = new Car("VOLVO", "2000");
        List<Car> cars = new ArrayList<>();
        cars.add(new Car("BMW", "1990"));
        cars.add(new Car("Audi", "1991"));
        cars.add(new Car("VOLVO", "2000"));
        cars.add(new Car("VOLVO", "2000"));
        //cars.add(volvo);
        //cars.add(volvo);
        cars.add(new Car("MAZDA", "2005"));

//        for (Car car : cars) {
//            System.out.println(car);
//        }
        //cars.forEach(car -> System.out.println(car));
        //cars.forEach(System.out::println);

        // SET уникальные данные -> делали свои equals() и hashCode() чтобы разные объекты с одинаковыми
        // значениями стали равны.
        Set<Car> carSet = new HashSet<>(cars);
        carSet.forEach(System.out::println);

        //java.util.ConcurrentModificationException в данном примере!
//        for (Car car : cars) {
//            if (car.getModel().equals("MAZDA")) {
//                cars.remove(car);
//            }
//        }

        //Лямбда по удалению элемента
        //cars.removeIf(car -> car.getModel().equals("MAZDA"));

        Iterator<Car> iterator = cars.iterator();
        while (iterator.hasNext()) {
            Car car = iterator.next();
            if (car.getModel().equals("MAZDA")) {
                iterator.remove();
            }
        }
        cars.forEach(System.out::println);
        Collections.sort(cars, new Comparator<Car>() {
            @Override
            public int compare(Car o1, Car o2) {
                return o1.getModel().compareTo(o2.getModel());
            }
        });

        Collections.sort(cars, (o1, o2) -> o1.getModel().compareTo(o2.getModel()));

        Collections.sort(cars, Comparator.comparing(Car::getModel));
        //cars.sort(Collections.reverseOrder());
        cars.forEach(System.out::println);
    }
}
