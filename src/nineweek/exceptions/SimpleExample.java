package nineweek.exceptions;

import java.util.Scanner;

//https://javastudy.ru/wp-content/uploads/2016/01/exceptionsInJavaHierarchy.png
public class SimpleExample {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
//        if (n != 0 ) {
//            System.out.println("число не должно равняться 0");
//            return;
//        }
        //System.out.println(100 / n);
//        try {
//            toDivide(n);
//        } catch (ArithmeticException e) {
//            throw new MyArithmeticException();
//        }
//        finally {
//            System.out.println("Last words of program");
//        }

        try {
            toDivide(n);
        } catch (MyArithmeticException e) {
//            throw new RuntimeException(e);
            System.out.println(e.getMessage());
        }
//        toDivide(n);
        System.out.println("Hello world!");
    }

    public static void toDivide(int n) throws MyArithmeticException {
        try {
            System.out.println(100 / n);
        } catch (ArithmeticException e) {
            throw new MyArithmeticException();
        }
        finally {
            System.out.println("Last words of program");
        }
    }
}
