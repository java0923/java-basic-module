package nineweek.exceptions;

public class MyArithmeticException extends Exception {
    //ArithmeticException {
    public MyArithmeticException() {
        super("Исключение деления на 0!!!!");
    }

    public MyArithmeticException(String errorMessage) {
        super(errorMessage);
    }
}
