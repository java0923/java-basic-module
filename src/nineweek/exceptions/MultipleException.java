package nineweek.exceptions;

public class MultipleException {
    public static void main(String[] args) {
        try {
            someMethodThrowArrayIndexOutOfBoundException();
        } catch (Throwable e) {
//            System.out.println("LOG: Деление на 0!");
            System.out.println("LOG: " + e.getMessage());
        }
//        catch (MyArithmeticException | RuntimeException e) {
//            System.out.println("LOG: " + e.getMessage());
//        }
        try {
            simpleThrowRuntimeException();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        try {
            toDivideThrowMyArithmeticException(100, 0);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void simpleThrowRuntimeException() {
        throw new RuntimeException("Runtime!");
    }

    public static void someMethodThrowArrayIndexOutOfBoundException() {
        int[] arr = new int[10];
        System.out.println(arr[10]);
    }

    public static void toDivideThrowMyArithmeticException(int a, int b) throws MyArithmeticException {
        try {
            System.out.println(a / b);
        } catch (ArithmeticException e) {
            throw new MyArithmeticException();
        }
    }
}
