package nineweek.exceptions.genericscollections.task1;

/*
Класс, который умеет хранить:
1) два значения любого типа Pair<T, U>
T, E, (K, V), U

2) два значения одинакового типа Pair<T>

3) первый параметр ТОЛЬКО String, второй ТОЛЬКО число (Pair<T extends ?, U extends ?>)
 */
public class Pair<T extends String, U extends Number> {
    public T first;
    public U second;

    public void print() {
        System.out.println("First: " + first + "; Second: " + second);
    }
}
