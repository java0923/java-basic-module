package nineweek.exceptions.genericscollections.task5;

import java.util.Set;
import java.util.TreeSet;

/*
С собеседований

На вход подается строка, состоит она из маленьких латинских символов.
Проверить, что в строке встречаются все символы английского алфавита хотя бы раз!

asdsadqdwe -> false
qwertyuiopasdfghjklzxcvbnmljhjqenb -> true
 */
public class Main {
    public static void main(String[] args) {
        System.out.println(checkString("asdsadqdwe"));
        System.out.println(checkString("qwertyuiopasdfghjklzxcvbnmljhjqenb"));
    }

    public static boolean checkString(String input) {
        if (input.length() < 26) {
            return false;
        }
        Set<Character> characters = new TreeSet<>();
        for (char ch : input.toCharArray()) {
            characters.add(ch);
        }
        return characters.size() == 26;
    }
}
