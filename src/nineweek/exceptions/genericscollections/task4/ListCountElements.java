package nineweek.exceptions.genericscollections.task4;

import java.util.List;

/*
Реализовать метод, который считает количество элементов в переданном списке.
 */
public class ListCountElements {
    public static <T> int countIf(List<T> from, T element) {
        int counter = 0;
        for (T elem : from) {
            if (elem.equals(element)) {
                ++counter;
            }
//            if (elem == element) {
//                ++counter;
//            }
        }
        return counter;
    }
}
