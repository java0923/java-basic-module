package nineweek.exceptions.genericscollections.task4;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
//        List<Boolean> list1 = new ArrayList<>();
//        list1.add(true);
//        list1.add(true);
//        list1.add(false);
//        System.out.println(ListCountElements.countIf(list1, true));
//        System.out.println(ListCountElements.countIf(list1, false));

        //TODO: разобрать пример с String!!! (пулы строк)
        //TODO: разобрать пример с Map и подсчетом слов (задача с собеседования)
        //На вход подается список слов (List<String>), на выходе нужно вернуть коллекцию (Map),
        //в ней хранить количество поданных на вход слов
        //List: слон, слон, кот, собака, мышь, мышь
        //Выход: (слон, 2), (кот, 1), (собака, 1), (мышь, 2)
        String str1 = "TopJava";
        String str2 = "TopJava";
        String str3 = (new String("TopJava")).intern();
        String str4 = (new String("TopJava")).intern();
        System.out.println("Строка 1 равна строке 2? " + (str1 == str2));
        System.out.println("Строка 2 равна строке 3? " + (str2 == str3));
        System.out.println("Строка 3 равна строке 4? " + (str3 == str4));

    }
}
