package nineweek.exceptions.genericscollections.task3;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/*
Есть два set.
Вывести уникальный набор данных, которые встречаются и в первом и во втором сете.
 */
public class Main {
    public static void main(String[] args) {
        Set<Integer> set1 = new HashSet<>();
        set1.add(0);
        set1.add(0);
        set1.add(1);
        set1.add(2);
        set1.add(3);
        set1.add(4);

        Set<Integer> set2 = new HashSet<>();
        set2.add(4);
        set2.add(3);
        set2.add(7);
        set2.add(8);

        set1.retainAll(set2);
        for (Integer i : set1) {
            System.out.println(i);
        }
        set1.forEach(System.out::println);
        //Вернет true если нет общих элементов в двух коллекциях
        //System.out.println(Collections.disjoint(set1, set2));
    }
}
