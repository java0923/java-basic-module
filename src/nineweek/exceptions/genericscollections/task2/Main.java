package nineweek.exceptions.genericscollections.task2;

public class Main {
    public static void main(String[] args) {
        Pair<String, Double> pair = new Pair<>("ABC", 12.2);
        pair.setSecond(4.3);
        System.out.println(pair);
    }
}
