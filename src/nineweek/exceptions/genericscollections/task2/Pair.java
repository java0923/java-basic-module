package nineweek.exceptions.genericscollections.task2;

/*
Реализовать getter/setter для класса Pair
 */
public class Pair<T extends String, U extends Number> {
    private T first;
    private U second;

    public Pair() {
    }

    public Pair(T first, U second) {
        this.first = first;
        this.second = second;
    }

    public T getFirst() {
        return this.first;
    }

    public void setFirst(T first) {
        this.first = first;
    }

    public U getSecond() {
        return this.second;
    }

    public void setSecond(U second) {
        this.second = second;
    }

    @Override
    public String toString() {
        return "Pair{" +
                "first='" + first + '\'' +
                ", second=" + second +
                '}';
    }
}
