package nineweek.exceptions.genericscollections.task6;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

//На вход подается список слов (List<String>), на выходе нужно вернуть коллекцию (Map),
//в ней хранить количество поданных на вход слов
//List: слон, слон, кот, собака, мышь, мышь
//Выход: (слон, 2), (кот, 1), (собака, 1), (мышь, 2)
public class WordCount {
    public static void main(String[] args) {
        List<String> words = new ArrayList<>();
        words.add("слон");
        words.add("слон");
        words.add("кот");
        words.add("собака");
        words.add("мышь");
        words.add("мышь");
        System.out.println(getWordsCount(words));
    }

    public static Map<String, Integer> getWordsCount(List<String> input) {
        Map<String, Integer> result = new HashMap<>();
        for (String word : input) {
            int counter = 1;
            if (result.containsKey(word)) {
                counter = result.get(word);
                counter++;
            }
            result.put(word, counter);
        }
        return result;
    }
//    Map<Object, Map<Integer, List<String>>> res;
}
