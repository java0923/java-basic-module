package sixweek.oop1.task2;

public enum TemperatureUnit {
    CELSIUS,
    FAHRENHEIT
}
