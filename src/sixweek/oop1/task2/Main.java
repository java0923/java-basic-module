package sixweek.oop1.task2;

public class Main {
    public static void main(String[] args) {
//        Thermometer thermometer = new Thermometer(37, "F");
//        System.out.println(thermometer.getTempCelsius());
//        System.out.println(thermometer.getTempFahrenheit());

        Thermometer thermometer1 = new Thermometer(37, TemperatureUnit.CELSIUS);
        System.out.println(thermometer1.getTempCelsius());
        System.out.println(thermometer1.getTempFahrenheit());
    }
}
