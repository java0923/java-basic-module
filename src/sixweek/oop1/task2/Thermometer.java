package sixweek.oop1.task2;

/*
Реализовать класс “Термометр”.
Необходимо иметь возможность создавать экземпляр класса с текущей температурой и
получать значение в фаренгейте и в цельсии.

currentTemperature * 1.8 + 32 - Фаренгейты
(currentTemperature - 32) / 1.8 - Цельсии
 */
public class Thermometer {
    private double tempCelsius;
    private double tempFahrenheit;

    public Thermometer(double currentTemperature) {
        tempCelsius = currentTemperature;
        tempFahrenheit = fromCelsiusToFahrenheit(currentTemperature);
    }

    public Thermometer(double currentTemperature, TemperatureUnit temperatureUnit) {
        if (temperatureUnit == TemperatureUnit.CELSIUS) {
            tempCelsius = currentTemperature;
            tempFahrenheit = fromCelsiusToFahrenheit(currentTemperature);
        } else if (temperatureUnit == TemperatureUnit.FAHRENHEIT) {
            tempFahrenheit = currentTemperature;
            tempCelsius = fromFahrenheitToCelsius(currentTemperature);
        }
    }

//    public Thermometer(double currentTemperature, String temperatureUnit) {
//        if (temperatureUnit.equals("C")) {
//            tempCelsius = currentTemperature;
//            tempFahrenheit = fromCelsiusToFahrenheit(currentTemperature);
//        } else if (temperatureUnit.equals("F")) {
//            tempFahrenheit = currentTemperature;
//            tempCelsius = fromFahrenheitToCelsius(currentTemperature);
//        } else {
//            System.out.println("Единица измерения температуры не распознана. По умолчанию = цельсий");
//            tempCelsius = currentTemperature;
//            tempFahrenheit = fromCelsiusToFahrenheit(currentTemperature);
////            System.exit(0);
////            throw new UnsupportedOperationException("Единица измерения температуры не распознана");
//        }
//    }

    private double fromCelsiusToFahrenheit(double currentTemperature) {
        return currentTemperature * 1.8 + 32;
    }

    private double fromFahrenheitToCelsius(double currentTemperature) {
        return (currentTemperature - 32) / 1.8;
    }

    public double getTempCelsius() {
        return this.tempCelsius;
    }

    public double getTempFahrenheit() {
        return this.tempFahrenheit;
    }
}
