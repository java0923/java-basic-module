package sixweek.oop1.task1;

//@Getter
//@Setter
//@RequiredArgsConstructor
public class Bulb {
    private boolean toggle;

    public Bulb() {
        this.toggle = false;
    }

    public Bulb(boolean toggle) {
        this.toggle = toggle;
    }

    public void turnOn() {
        this.toggle = true;
    }

    public void turnOff() {
        this.toggle = false;
    }

    public boolean isShining() {
        return this.toggle;
    }
}
