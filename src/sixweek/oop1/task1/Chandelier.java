package sixweek.oop1.task1;

/*
Включать/выключать люстру.
У люстры много лампочек
 */
public class Chandelier {
    private Bulb[] chandelier;

    public Chandelier(int countOfBulbs) {
        chandelier = new Bulb[countOfBulbs];
        for (int i = 0; i < countOfBulbs; i++) {
            chandelier[i] = new Bulb();
        }
    }

    public void turnOn() {
        for (Bulb bulb : chandelier) {
            bulb.turnOn();
        }
    }

    public void turnOff() {
        for (Bulb bulb : chandelier) {
            bulb.turnOff();
        }
    }

    public boolean isShining() {
        return chandelier[0].isShining();
    }
}
