package sixweek.oop1.task1;

/*
Создать класс реализующий функционал Лампочки.
Учесть ее состояния и свойства.
 */
public class Main {
    public static void main(String[] args) {
        Bulb bulb = new Bulb(true);
//        Bulb bulb1 = new Bulb();
        //bulb.toggle = false;
        bulb.turnOff();

        System.out.println("Светит ли сейчас лампочка? " + bulb.isShining());
        bulb.turnOff();
        System.out.println("Светит ли сейчас лампочка? " + bulb.isShining());
        bulb.turnOn();
        System.out.println("Светит ли сейчас лампочка? " + bulb.isShining());

        Chandelier chandelier = new Chandelier(4);
        chandelier.turnOn();
        System.out.println("Светит ли сейчас люстра? " + chandelier.isShining());
        chandelier.turnOff();
    }
}
