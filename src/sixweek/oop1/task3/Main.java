package sixweek.oop1.task3;
public class Main {
    public static void main(String[] args) {
        System.out.println(FieldValidator.validateEmail("andy_12@bk.ru"));
        System.out.println(FieldValidator.validatePhone("+79214040998"));
        System.out.println(FieldValidator.validateName("Andrei"));
        System.out.println(FieldValidator.validateBirthDate("11.10.1990"));
    }
}
